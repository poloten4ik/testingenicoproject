//
//  PAX.h
//  PAX
//
//  Created by админ on 28.04.16.
//  Copyright © 2016 m4bank. All rights reserved.
//

#import "PAXDeviceManagerInterface.h"
#import "PAXConfigurationManagerInterface.h"
#import "PAXTransactionManagerInterface.h"

//! Project version number for PAX.
FOUNDATION_EXPORT double PAXVersionNumber;

//! Project version string for PAX.
FOUNDATION_EXPORT const unsigned char PAXVersionString[];

@interface PAX : NSObject

+ (id) sharedInstance;

- (id <PAXDeviceManagerInterface>) deviceManager;

- (id <PAXConfigurationManagerInterface>) configurationManager;

- (id <PAXTransactionManagerInterface>) transactionManager;

@end
