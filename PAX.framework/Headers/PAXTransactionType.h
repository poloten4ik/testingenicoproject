//
//  PAXTransactionType.h
//  PAX
//
//  Created by админ on 26.05.16.
//  Copyright © 2016 m4bank. All rights reserved.
//

typedef NS_ENUM(int, PAXTransactionType) {
    PAXTransactionTypeUnknown = 0,
    PAXTransactionTypePayment = 0x01,
    PAXTransactionTypeRefund = 0x03,
    PAXTransactionTypeCloseOperday = 0x07,
    PAXTransactionTypeShowServiceMenu = 0x0B,
    PAXTransactionTypeCancelLastPayment = 0x0D
};
