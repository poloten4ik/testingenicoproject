//
//  PAXDeviceManagerInterface.h
//  PAX
//
//  Created by админ on 26.05.16.
//  Copyright © 2016 m4bank. All rights reserved.
//

#import "PAXDevice.h"

typedef void(^FindDeviceCompletion)(NSArray <PAXDevice *> * foundDevices);
typedef void(^ConnectionBlock)(BOOL connected);

@protocol PAXDeviceManagerInterface <NSObject>

@property (nonatomic, readonly) BOOL isConnected;

@property (nonatomic, assign) NSTimeInterval timeout; //default 30 seconds

@property (nonatomic, copy) ConnectionBlock connectionStateChangeBlock;

- (void) findDevicesWithCompletion:(FindDeviceCompletion) completion;
- (void) cancelDeviceSearch;

- (void) connectDevice:(PAXDevice*)device;
- (void) disconnect;

@end