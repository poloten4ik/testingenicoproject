//
//  PAXConfigurationManager.h
//  PAX
//
//  Created by админ on 26.05.16.
//  Copyright © 2016 m4bank. All rights reserved.
//

#import "PAXDeviceInfo.h"

typedef void(^PAXStringInfoCompletion)(NSString* deviceInfo);
typedef void(^PAXDeviceInfoCompletion)(PAXDeviceInfo* deviceInfo);

@protocol PAXConfigurationManagerInterface <NSObject>

- (void) getDeviceInfoWithCompletion:(PAXDeviceInfoCompletion)completion;

- (void) getTerminalNumberWithCompletion:(PAXStringInfoCompletion)completion;
- (void) getMerchantNumberWithCompletion:(PAXStringInfoCompletion)completion;

@end
