//
//  PAXCardReadMethod.h
//  PAX
//
//  Created by VS on 17.08.16.
//  Copyright © 2016 m4bank. All rights reserved.
//

typedef NS_ENUM(int, PAXCardReadMethod) {
    
    PAXCardReadMethodMagnet = 0,
    PAXCardReadMethodManualEntry = 1,
    PAXCardReadMethodChip = 2,
    PAXCardReadMethodFallback = 3,
    PAXCardReadMethodContactlessMagnet = 4,
    PAXCardReadMethodContactlessChip = 5,
    PAXCardReadMethodClientId = 6
};
