//
//  PAXTransactionFlags.h
//  PAX
//
//  Created by VS on 17.08.16.
//  Copyright © 2016 m4bank. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAXCardReadMethod.h"
#import "PAXTransactionVerificationMethod.h"

@interface PAXTransactionFlags : NSObject

@property (nonatomic, readonly) PAXCardReadMethod readMethod;
@property (nonatomic, readonly) PAXTransactionVerificationMethod verificationMethod;

@end
