//
//  PAXTransactionResults.h
//  PAX
//
//  Created by админ on 26.05.16.
//  Copyright © 2016 m4bank. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAXTransactionFlags.h"

@interface PAXTransactionData : NSObject

@property (nonatomic, readonly) int resultCode;
@property (nonatomic, readonly) NSString * resultDescription;

@property (nonatomic, readonly) NSString * authorizationCode;

@property (nonatomic, readonly) NSString * RRN;
@property (nonatomic, readonly) NSString * operationNumber; //0001-9999
@property (nonatomic, readonly) NSString * terminalNumber;

@property (nonatomic, readonly) NSString * cardNumber;
@property (nonatomic, readonly) NSString * cardExpDate; //MM/YY
@property (nonatomic, readonly) NSString * cardType; //"Visa", "Maestro" etc.
@property (nonatomic, readonly) NSString * cardNumberHash; //SHA1

@property (nonatomic, readonly) int operationDate; //YYYYMMDD
@property (nonatomic, readonly) int operationTime; //HHMMSS

@property (nonatomic, readonly) int requestId;
@property (nonatomic, readonly) int cardId;

@property (nonatomic, readonly) int operationType;

@property (nonatomic, readonly) BOOL cardEmittedBySberbank;

@property (nonatomic, readonly) PAXTransactionFlags * transactionFlags;

@property (nonatomic, readonly) NSString * receipt;

@end
