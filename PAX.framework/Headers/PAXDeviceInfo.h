//
//  PAXDeviceInfo.h
//  PAX
//
//  Created by VS on 24.03.17.
//  Copyright © 2017 m4bank. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAXDeviceInfo : NSObject

@property (nonatomic, readonly) NSString * rawString;

@property (nonatomic, readonly) NSString * vendor;
@property (nonatomic, readonly) NSString * version;
@property (nonatomic, readonly) NSString * serial;
@property (nonatomic, readonly) NSString * model;

- (instancetype) initWithString:(NSString*)string;

@end
