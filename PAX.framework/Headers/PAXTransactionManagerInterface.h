//
//  PAXTransactionManager.h
//  PAX
//
//  Created by админ on 26.05.16.
//  Copyright © 2016 m4bank. All rights reserved.
//

#import "PAXTransactionData.h"

typedef void(^PAXTransactionCompletion)(PAXTransactionData * transactionData);

@protocol PAXTransactionManagerInterface <NSObject>

- (void) paymentWithAmount:(unsigned long long)amount completion:(PAXTransactionCompletion)completion;
- (void) refundWithAmount:(unsigned long long)amount originalTransactionRRN:(NSString*)originalRRN completion:(PAXTransactionCompletion)completion;
- (void) cancelLastPaymentWithCompletion:(PAXTransactionCompletion)completion;
- (void) closeOperdayWithCompletion:(PAXTransactionCompletion)completion;
- (void) showServiceMenu;

@end
