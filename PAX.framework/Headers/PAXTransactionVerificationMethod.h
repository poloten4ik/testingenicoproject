//
//  PAXTransactionVerificationMethod.h
//  PAX
//
//  Created by VS on 17.08.16.
//  Copyright © 2016 m4bank. All rights reserved.
//

typedef NS_ENUM(NSUInteger, PAXTransactionVerificationMethod) {
    
    PAXTransactionVerificationMethodUnknown = 0,
    PAXTransactionVerificationMethodBio = 0x80,
    PAXTransactionVerificationMethodPinOnline = 0x100,
    PAXTransactionVerificationMethodPinOffline = 0x200,
    PAXTransactionVerificationMethodNotRequired = 0x400
    
};
