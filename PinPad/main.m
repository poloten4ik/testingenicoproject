//
//  main.m
//  PinPad
//
//  Created by Mikhail Zaslavskiy on 9/14/17.
//  Copyright © 2017 Mikhail Zaslavskiy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
