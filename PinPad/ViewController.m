//
//  ViewController.m
//  PinPad
//
//  Created by Mikhail Zaslavskiy on 9/14/17.
//  Copyright © 2017 Mikhail Zaslavskiy. All rights reserved.
//

#import "ViewController.h"
#import <SBUnifiedReaderAPI/SBUnifiedReaderAPI.h>

@interface ViewController ()

@property (nonatomic, strong) id <SBDeviceManagerInterface> currentDeviceManager;
@property (nonatomic, assign) BOOL readerConnected;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.readerConnected = NO;
    self.currentDeviceManager = [SBUnifiedReaderAPI deviceManagerForType:SBDeviceTypeICMPUpos];
}

- (IBAction)search:(id)sender {
    id <SBConnectionManagerInterface> connectionManager = [self.currentDeviceManager connectionManager];
    
    [self disconnectCurrentDevice];
    
    [connectionManager searchDevicesWithSuccess:^(NSArray<SBDevice *>
                                                  *findedDevices) {
        //В процессе выполнения, если ответ должен быть положительным - получаем краш. Должен вызваться этот блок, но он не вызывается так как крашится программа.
        SBDevice * firstDevice = findedDevices.firstObject;
        [connectionManager connectDevice:firstDevice];
        NSLog(@"SUCCESS");
    } failure:^(NSError *error) {
        NSLog(@"ERROR");
    }];
}

- (void) disconnectCurrentDevice
{
    [[self.currentDeviceManager connectionManager] disconnectDevice];
}


@end
