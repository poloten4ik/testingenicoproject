//
//  PAXTransactionManager.h
//  PAX
//
//  Created by админ on 26.05.16.
//  Copyright © 2016 m4bank. All rights reserved.
//

#import "iCMPUposTransactionData.h"

typedef void(^iCMPUposTransactionCompletion)(iCMPUposTransactionData * transactionData);
typedef void(^iCMPUposCompletion)();
typedef void(^iCMPUposTransactionFailureBlock)(NSError * error);

@protocol iCMPUposTransactionManagerInterface <NSObject>

@property (nonatomic, readwrite) NSString * merchantId;

- (void) paymentWithAmount:(unsigned long long)amount currencyCode:(NSUInteger)currencyCode completion:(iCMPUposTransactionCompletion)completion failure:(iCMPUposTransactionFailureBlock)failure;
- (void) refundWithAmount:(unsigned long long)amount currencyCode:(NSUInteger)currencyCode originalTransactionRRN:(NSString*)originalRRN completion:(iCMPUposTransactionCompletion)completion failure:(iCMPUposTransactionFailureBlock)failure;
- (void) reversalWithAmount:(unsigned long long)amount currencyCode:(NSUInteger)currencyCode originalTransactionRRN:(NSString*)originalRRN completion:(iCMPUposTransactionCompletion)completion failure:(iCMPUposTransactionFailureBlock)failure;
- (void) reversalLastPaymentWithCompletion:(iCMPUposTransactionCompletion)completion failure:(iCMPUposTransactionFailureBlock)failure;
- (void) closeOperdayWithCompletion:(iCMPUposTransactionCompletion)completion failure:(iCMPUposTransactionFailureBlock)failure;
- (void) showServiceMenuWithCompletion:(iCMPUposCompletion)completion failure:(iCMPUposTransactionFailureBlock)failure;

@end
