//
//  iCMPDeviceManager.h
//  iCMPCass
//
//  Created by админ on 04.12.14.
//  Copyright (c) 2014 AOW. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "iCMPUposDevice.h"

#import "iCMPUposTransactionManagerInterface.h"

@protocol iCMPUposDeviceManagerDelegate <NSObject>

@required
- (void) devicesDidFinded:(NSArray*)devices;
- (void) devicesDidNotFinded;

- (void) deviceDidConnected:(iCMPUposDevice*)device;
- (void) deviceDidDisconnected:(iCMPUposDevice*)device;

- (void) device:(iCMPUposDevice*)device connectionError:(NSError*)error;

@end

@interface iCMPUposDeviceManager : NSObject

@property (nonatomic, weak) id <iCMPUposDeviceManagerDelegate> delegate;
@property (nonatomic, assign) NSUInteger port; // default 9301
@property (nonatomic, assign) BOOL connected;

@property (nonatomic, readonly) id <iCMPUposTransactionManagerInterface> transactionManager;

+ (iCMPUposDeviceManager *)sharedDeviceManager;

- (void) findDevicesWithDelegate:(id<iCMPUposDeviceManagerDelegate>)delegate;

- (void) connectDevice:(iCMPUposDevice*)device;
- (void) disconnect;

- (NSString*) getKSN;


@end
