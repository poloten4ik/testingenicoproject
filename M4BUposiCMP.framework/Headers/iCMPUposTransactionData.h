//
//  iCMPUposTransactionData.h
//  M4BiCMP
//
//  Created by VS on 29.11.16.
//  Copyright © 2016 M4Bank. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "iCMPUposTransactionCardFormat.h"
#import "iCMPUposCardBrand.h"

@interface iCMPUposTransactionData : NSObject

@property (nonatomic, strong) NSString * operationNumber;

@property (nonatomic, strong) NSString * terminalId;
@property (nonatomic, strong) NSString * responseCode;
@property (nonatomic, strong) NSString * hostResponseCode;
@property (nonatomic, strong) NSString * authorizationCode;

@property (nonatomic, strong) NSString * pan;
@property (nonatomic, strong) NSString * cardHolder;
@property (nonatomic, strong) NSString * expDate;
@property (nonatomic, assign) iCMPUposCardBrand cardBrand;

@property (nonatomic, strong) NSString * receipt;
@property (nonatomic, strong) NSString * rrn;

@property (nonatomic, strong) NSString * cvm;
@property (nonatomic, assign) iCMPUposTransactionCardFormat cardFormat;

@end
