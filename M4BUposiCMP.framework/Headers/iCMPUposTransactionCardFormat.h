//
//  iCMPUposTransactionCardFormat.h
//  M4BiCMP
//
//  Created by VS on 06.12.16.
//  Copyright © 2016 M4Bank. All rights reserved.
//

typedef NS_ENUM(NSUInteger, iCMPUposTransactionCardFormat)
{
    iCMPUposTransactionCardFormatUnknown,
    iCMPUposTransactionCardFormatManualEntry,
    iCMPUposTransactionCardFormatMagStripe,
    iCMPUposTransactionCardFormatEMV,
    iCMPUposTransactionCardFormatConcactlessEMV,
    iCMPUposTransactionCardFormatConcactlessMagStripe,
    iCMPUposTransactionCardFormatMagStripeFallback
};
