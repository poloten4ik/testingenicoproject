//
//  iCMPUposCardBrand.h
//  M4BiCMP
//
//  Created by VS on 10.04.17.
//  Copyright © 2017 M4Bank. All rights reserved.
//

typedef NS_ENUM(NSUInteger, iCMPUposCardBrand)
{
    iCMPUposCardBrandUnknown,
    iCMPUposCardBrandVisa,
    iCMPUposCardBrandVisaElectron,
    iCMPUposCardBrandMastercard,
    iCMPUposCardBrandMaestro,
    iCMPUposCardBrandMIRDebit,
    iCMPUposCardBrandMIRCredit
};
