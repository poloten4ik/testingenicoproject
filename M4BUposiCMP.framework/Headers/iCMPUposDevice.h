//
//  iCMPDevice.h
//  iCMPCass
//
//  Created by админ on 04.12.14.
//  Copyright (c) 2014 AOW. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface iCMPUposDevice : NSObject

- (instancetype)initWithId:(NSString*)deviceId;

@property (nonatomic, readonly) NSString * deviceId;

@end
