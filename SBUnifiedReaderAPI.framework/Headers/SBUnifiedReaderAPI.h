//
//  SBUnifiedReaderAPI.h
//  SBUnifiedReaderAPI
//
//  Created by VS on 31.05.17.
//  Copyright © 2017 CCT. All rights reserved.
//

#import "SBDeviceManagerInterface.h"

// SBUnifiedReaderAPIVersion : v1.0

//! Project version number for SBUnifiedReaderAPI.
FOUNDATION_EXPORT double SBUnifiedReaderAPIVersionNumber;

//! Project version string for SBUnifiedReaderAPI.
FOUNDATION_EXPORT const unsigned char SBUnifiedReaderAPIVersionString[];

@interface SBUnifiedReaderAPI : NSObject

+ (NSArray *) supportedDevices;

+ (id <SBDeviceManagerInterface>) deviceManagerForType:(SBDeviceType)type;

@end


