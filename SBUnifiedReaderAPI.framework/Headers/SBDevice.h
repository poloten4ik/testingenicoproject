//
//  SBDevice.h
//  SberbankReader unified API
//
//  Created by VS on 01.06.17.
//  Copyright © 2017 CCT. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SBDeviceType.h"

@interface SBDevice : NSObject

@property (nonatomic, readonly) SBDeviceType type;

@property (nonatomic, readonly) NSString * name;

- (instancetype)initWithType:(SBDeviceType)type name:(NSString*)name;

@end
