//
//  SBConnectionManagerInterface.h
//  SberbankReader unified API
//
//  Created by VS on 31.05.17.
//  Copyright © 2017 CCT. All rights reserved.
//

#import "SBDevice.h"

typedef void (^SBDeviceSearchSuccessBlock)(NSArray <SBDevice *> * findedDevices);
typedef void (^SBDeviceSearchFailureBlock)(NSError * error);

typedef void (^SBDeviceConnectionStateBlock)(SBDevice * device);


@protocol SBConnectionManagerInterface <NSObject>

- (BOOL) deviceIsConnected;

- (void) searchDevicesWithSuccess:(SBDeviceSearchSuccessBlock)success failure:(SBDeviceSearchFailureBlock)failure;
- (void) cancelDeviceSearch;

- (void) connectDevice:(SBDevice*)device;
- (void) disconnectDevice;

- (void) setConnectActionBlock:(SBDeviceConnectionStateBlock)block;
- (void) setDisconnectActionBlock:(SBDeviceConnectionStateBlock)block;

@end

