//
//  SBVerificationMethod.h
//  SberbankReader unified API
//
//  Created by VS on 05.06.17.
//  Copyright © 2017 CCT. All rights reserved.
//

typedef NS_ENUM(NSUInteger, SBVerificationMethod) {
    SBVerificationMethodUnknown,
    SBVerificationMethodSignature,
    SBVerificationMethodPINOffline,
    SBVerificationMethodPINOnline,
    SBVerificationMethodNotRequired
};
