//
//  SBCardReadMethod.h
//  SberbankReader unified API
//
//  Created by VS on 05.06.17.
//  Copyright © 2017 CCT. All rights reserved.
//

typedef NS_ENUM(NSUInteger, SBCardReadMethod) {
    SBCardReadMethodUnknown,
    SBCardReadMethodManualEntry,
    SBCardReadMethodMagStripe,
    SBCardReadMethodEMV,
    SBCardReadMethodConcactlessEMV,
    SBCardReadMethodConcactlessMagStripe,
    SBCardReadMethodMagStripeFallback
};
