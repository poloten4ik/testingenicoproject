//
//  SBDeviceType.h
//  SberbankReader unified API
//
//  Created by VS on 31.05.17.
//  Copyright © 2017 CCT. All rights reserved.
//

typedef NS_ENUM(NSUInteger, SBDeviceType) {
    SBDeviceTypeUnknown = 0,
    SBDeviceTypePAXD200,
    SBDeviceTypeICMPUpos
};
