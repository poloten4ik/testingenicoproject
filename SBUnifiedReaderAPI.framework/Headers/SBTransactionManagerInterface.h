//
//  SBTransactionManagerInterface.h
//  SberbankReader unified API
//
//  Created by VS on 01.06.17.
//  Copyright © 2017 CCT. All rights reserved.
//

#import "SBTransactionData.h"

typedef void(^SBTransactionSuccessBlock)(SBTransactionData * data);
typedef void(^SBTransactionFailureBlock)(NSError * error);

@protocol SBTransactionManagerInterface <NSObject>

- (void) payWithAmount:(unsigned long long)amount currencyCode:(NSUInteger)currencyCode success:(SBTransactionSuccessBlock)success failure:(SBTransactionFailureBlock)failure;
- (void) refundWithAmount:(unsigned long long)amount currencyCode:(NSUInteger)currencyCode originalTransactionRRN:(NSString*)originalRRN success:(SBTransactionSuccessBlock)success failure:(SBTransactionFailureBlock)failure;
- (void) reverseWithAmount:(unsigned long long)amount currencyCode:(NSUInteger)currencyCode originalTransactionRRN:(NSString*)originalRRN success:(SBTransactionSuccessBlock)success failure:(SBTransactionFailureBlock)failure;
- (void) reverseLastPaymentWithSuccess:(SBTransactionSuccessBlock)success failure:(SBTransactionFailureBlock)failure;

- (void) closeOperdayWithSuccess:(SBTransactionSuccessBlock)success failure:(SBTransactionFailureBlock)failure;

@end
