//
//  SBDeviceManagerInterface.h
//  SberbankReader unified API
//
//  Created by VS on 05.06.17.
//  Copyright © 2017 CCT. All rights reserved.
//

#import "SBConnectionManagerInterface.h"
#import "SBConfigurationManagerInterface.h"
#import "SBTransactionManagerInterface.h"

@protocol SBDeviceManagerInterface <NSObject>

- (id <SBConnectionManagerInterface>) connectionManager;

- (id <SBConfigurationManagerInterface>) configurationManager;

- (id <SBTransactionManagerInterface>) transactionManager;

@end
