//
//  SBTransactionData.h
//  SberbankReader unified API
//
//  Created by VS on 01.06.17.
//  Copyright © 2017 CCT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SBCardBrand.h"
#import "SBCardReadMethod.h"
#import "SBVerificationMethod.h"

@interface SBTransactionData : NSObject

@property (nonatomic, readonly) NSString * terminalId;
@property (nonatomic, readonly) NSString * authorizationCode;
@property (nonatomic, readonly) NSString * operationNumber;
@property (nonatomic, readonly) NSString * RRN;

@property (nonatomic, readonly) NSString * PAN;
@property (nonatomic, readonly) NSString * expireDate;
@property (nonatomic, readonly) NSString * cardHolder;

@property (nonatomic, readonly) SBCardBrand cardBrand;
@property (nonatomic, readonly) SBCardReadMethod readMethod;
@property (nonatomic, readonly) SBVerificationMethod verificationMethod;

@property (nonatomic, readonly) NSString * receipt;

@property (nonatomic, readonly) NSString * responseCode;
@property (nonatomic, readonly) NSString * responseDescription;

@property (nonatomic, readonly) NSString * hostResponseCode;

@end

