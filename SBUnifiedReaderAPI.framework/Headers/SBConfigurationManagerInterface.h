//
//  SBConfigurationManagerInterface.h
//  SberbankReader unified API
//
//  Created by VS on 01.06.17.
//  Copyright © 2017 CCT. All rights reserved.
//

typedef void(^SBConfigurationInfoSuccessBlock)(id deviceInfo);
typedef void(^SBConfigurationStringSuccessBlock)(NSString * string);

typedef void(^SBConfigurationFailureBlock)(NSError * error);


@protocol SBConfigurationManagerInterface <NSObject>

- (void) getDeviceInfoWithSuccess:(SBConfigurationInfoSuccessBlock)success failure:(SBConfigurationFailureBlock)failure;

- (void) getTerminalNumberWithSuccess:(SBConfigurationStringSuccessBlock)success failure:(SBConfigurationFailureBlock)failure;
- (void) getMerchantNumberWithSuccess:(SBConfigurationStringSuccessBlock)success failure:(SBConfigurationFailureBlock)failure;

- (void) showServiceMenu;

@end
